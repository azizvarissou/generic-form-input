import { FormControl, FormGroup, Validators } from '@angular/forms';

import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'forms';
  fg: FormGroup;

  constructor() {
    this.fg = new FormGroup({
      email: new FormControl('', [Validators.email, Validators.required])
    });
  }

  get email() {
    return this.fg.get('email');
  }
}
