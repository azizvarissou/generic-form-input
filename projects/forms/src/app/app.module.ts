import { GenericFormInputComponent, GenericFormInputModule } from 'generic-form-input';

import { AppComponent } from './app.component';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    GenericFormInputModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
