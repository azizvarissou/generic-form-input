# GenericFormInput

This library was generated with [Angular CLI](https://github.com/angular/angular-cli) version 9.1.12.

This is an library to help you to manage directly a form validation on every angular projects.

# How to install

**npm install generic-form-input**

First, update your component with your form declaration in your component like this:
![Texte alternatif](https://ik.imagekit.io/pfnn8tg1sb/carbon_cH2sG3dPou-.png "form.component.ts example")

And after declare this in template with your personnal variable where you want to call a component.

![Texte alternatif](https://ik.imagekit.io/pfnn8tg1sb/carbon__1__tryUsc-lk.png "Declaration example in app.component.html")

<!-- ## Code scaffolding

Run `ng generate component component-name --project generic-form-input` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module --project generic-form-input`.
> Note: Don't forget to add `--project generic-form-input` or else it will be added to the default project in your `angular.json` file.

## Build

Run `ng build generic-form-input` to build the project. The build artifacts will be stored in the `dist/` directory.

## Publishing

After building your library with `ng build generic-form-input`, go to the dist folder `cd dist/generic-form-input` and run `npm publish`.

## Running unit tests

Run `ng test generic-form-input` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Further help -->

<!-- To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md). -->
