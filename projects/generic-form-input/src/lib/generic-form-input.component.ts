import { Component, Input, OnInit } from '@angular/core';

import { FormControl } from '@angular/forms';

@Component({
  selector: 'lib-generic-form-input',
  template: `
    <div class="input-form">
        <input [type]="inputType" [placeholder]="placeHolder" [formControl]="inputName"/>
      <div class="message">
        <div class="success" *ngIf="inputName.valid">{{successMessage}}</div>
        <div class="error" *ngIf="haveErrors()">
        <p *ngIf="inputName.errors.required">{{requiredMessage}}</p>
        <p *ngIf="inputName.invalid && (inputName.touched || inputName.dirty)">{{customErrorMessage}}</p>
        </div>
       </div>
    </div>
  `,
  styles: [
    `
    .input-form{
      display:flex;
      flex-direction:column;
      width:auto;
      margin:1% auto;
    }
    input{
      height:3vh;
      border-radius:30px;
      padding-left:10px;
      padding-right:10px;
    }
    input:focus{
      outline: none;
    }
    .message{
      padding-bottom:10px;
      width:auto;
      height:auto;
    }
    .error{
      background-color:#FE5B06;
      color:white;
      font-size:18px;
      border-radius:15px;
      width:auto;
      min-width:220px;
      transition:all 0.5s 5s ease;
      padding-left:20px;
      font-weight:bold;
    }

    .success{
      background-color:#55BA8E;
      color:white;
      font-size:18px;
      border-radius:10px;
      min-width:200px;
      width:300px;
      margin-top:20px;
      transition:all 0.5s 5s ease;
      padding-left:20px;
      font-weight:bold;
    }
  `]
})
export class GenericFormInputComponent implements OnInit {

  @Input() inputName: FormControl;
  @Input() placeHolder: string | number;
  @Input() inputType: string;
  @Input() requiredMessage?: string = 'Ce champ est recquis';
  @Input() customErrorMessage?: string = 'La saisie est incorrect';
  @Input() successMessage?: string = 'Champs correct, Continuer...';

  constructor() { }

  ngOnInit(): void {
  }
  haveErrors() {
    const { dirty, touched, errors } = this.inputName;
    return touched && dirty && errors;
  }

}
