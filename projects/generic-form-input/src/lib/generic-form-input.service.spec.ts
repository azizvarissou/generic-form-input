import { TestBed } from '@angular/core/testing';

import { GenericFormInputService } from './generic-form-input.service';

describe('GenericFormInputService', () => {
  let service: GenericFormInputService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(GenericFormInputService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
