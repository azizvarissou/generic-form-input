import { CommonModule } from '@angular/common';
import { GenericFormInputComponent } from './generic-form-input.component';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [GenericFormInputComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule
  ],
  exports: [GenericFormInputComponent]
})
export class GenericFormInputModule { }
