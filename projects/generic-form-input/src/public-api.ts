/*
 * Public API Surface of generic-form-input
 */

export * from './lib/generic-form-input.service';
export * from './lib/generic-form-input.component';
export * from './lib/generic-form-input.module';
